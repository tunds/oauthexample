//
//  RootViewController.swift
//  OAuthSwiftExample
//
//  Created by Tunde Adegoroye on 07/07/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import SwiftyJSON

class RootViewController: UIViewController {

    let realmMethods = RealmData()
    let spinnerMethods = Spinner()
    var tracks = SwiftyJSON.JSON([])
}
