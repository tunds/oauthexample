//
//  LoginViewController.swift
//  OAuthSwiftExample
//
//  Created by Tunde Adegoroye on 08/07/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import OAuthSwift
import SwiftyJSON
import Spring

class LoginViewController: RootViewController {

    
    let authorizeUrl = "https://soundcloud.com/connect"
    let accessToken = "https://api.soundcloud.com/oauth2/token"
    let callBackUrl = "oauthswiftexample://oauth-callback/soundcloud/"
    
    @IBOutlet weak var loginBtn: DesignableButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    @IBAction func LoginBtnDidTouch(sender: AnyObject) {
        
        
        
        loginBtn.animation = "pop"
        loginBtn.curve = "easeInOut"
        loginBtn.duration = 0.5
        loginBtn.damping = 0.4
        loginBtn.velocity = 0.3
        loginBtn.animate()
        doOAuthSoundCloud("20bc9e7db8e40065bc0331077326aa9e", consumerSecret: "b50d65498c712c0221a31706c0c210a1")
    }
    
    // MARK: FUNCTIONS
    func doOAuthSoundCloud(consumerKey: String, consumerSecret: String) {
        
        
        let oauthswift = OAuth2Swift(
            consumerKey:    consumerKey,
            consumerSecret: consumerSecret,
            authorizeUrl:   authorizeUrl,
            accessTokenUrl: accessToken,
            responseType:   "code"
        )
        
        oauthswift.authorize_url_handler = SafariURLHandler(viewController: self)
       // oauthswift.authorize_url_handler = AuthWebViewController()
        
        let state: String = generateStateWithLength(20) as String
        oauthswift.authorizeWithCallbackURL( NSURL(string: callBackUrl)!, scope: "", state: state, success: {
            credential, response, parameters in
            self.saveUserData(oauthswift,credential.oauth_token)
          
            
            }, failure: { error in
                print(error.localizedDescription)
        })
    }
    
    
    func saveUserData(oauthswift: OAuth2Swift, _ oauthToken: String) {
        oauthswift.client.get("https://api.soundcloud.com/me?oauth_token=\(oauthToken)",
                              success: {
                                data, response in
                                let jsonResponse = JSON(data: data)
                              
                                if let userName = jsonResponse["full_name"].string,
                                       id = jsonResponse["id"].int
                                {
                                  
                                    let avatar_url = jsonResponse["avatar_url"].string!
                                     let desc = jsonResponse["description"].string!
                                    
                                    self.realmMethods.addDefaultUserInfo(id, name: userName, avatar_url: avatar_url, description: desc, accessToken: oauthToken)
                                    self.performSegueWithIdentifier("ProfileSegue", sender: nil)
                                }
                                
                                
            }, failure: { error in
                print(error)
        })
    }

}
