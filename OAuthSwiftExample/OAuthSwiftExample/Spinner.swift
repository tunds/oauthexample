//
//  Spinner.swift
//  OAuthSwiftExample
//
//  Created by Tunde Adegoroye on 07/07/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import SwiftLoader

class Spinner: UIViewController {

    
    func ShowLoader(){
        
        
        var config : SwiftLoader.Config = SwiftLoader.Config()
        config.size = 170
        config.backgroundColor = .whiteColor()
        config.spinnerColor = .orangeColor()
        config.titleTextColor = .orangeColor()
        config.spinnerLineWidth = 2.0
        config.foregroundColor = UIColor.blackColor()
        config.foregroundAlpha = 0.5
        
        SwiftLoader.setConfig(config)
        
        SwiftLoader.show(title: "Loading...", animated: true)
    }
    
    func hideLoader() {
        SwiftLoader.hide()
    }
}
