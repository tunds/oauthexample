//
//  TrackTableViewCell.swift
//  OAuthSwiftExample
//
//  Created by Tunde Adegoroye on 09/07/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import Spring
import SwiftyJSON

class TrackTableViewCell: UITableViewCell {
    
    
    let realmMethods = RealmData()
    
    @IBOutlet weak var trackArtworImgVw: DesignableImageView!
    @IBOutlet weak var trackTitleLbl: UILabel!
    @IBOutlet weak var trackDescLbl: UILabel!
    @IBOutlet weak var trackAuthorLbl: UILabel!
    @IBOutlet weak var trackPlayButton: DesignableButton!
    
    var trackBtnIndexPath: NSIndexPath?
    
    func configureTrack(track: JSON, indexPath: NSIndexPath) {

        let title = track["title"].string!
        let author = track["user"]["username"].string!
        let artwork = track["artwork_url"].string!
        let desc = track["description"].string!
        
        
        trackTitleLbl.text = title
        trackDescLbl.text = desc
        trackAuthorLbl.text = author
        trackPlayButton.tag = indexPath.row
        trackArtworImgVw.image = realmMethods.getImageFromURL(artwork)
        
        //        // Configure the controls
        //        newsStoryLbl.text = title
        //        newsStoryLbl.textColor = UIColor(red:0, green:0, blue:0, alpha:1)
        //
        //        newsSourceTitleLbl.text = source
        //
        //        newsSourceImgVw.image = UIImage(named: icon)
        //        newsSourceImgVw.restorationIdentifier = icon
        //
        //        newsTimePostedLbl.text = time
        //
        //        // Attach the unique indexpath to the button
        //        bookmarkButton.bookmarkIndexPath = indexPath
        
    }
    
    
    
}
