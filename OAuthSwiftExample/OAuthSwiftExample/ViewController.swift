//
//  ViewController.swift
//  OAuthSwiftExample
//
//  Created by Tunde Adegoroye on 07/07/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import SwiftLoader
import OAuthSwift
import Spring
import SwiftyJSON
import SafariServices
import DZNEmptyDataSet


class ViewController: RootViewController, UITableViewDelegate, UITableViewDataSource, SFSafariViewControllerDelegate, DZNEmptyDataSetDelegate, DZNEmptyDataSetSource{
    
    @IBOutlet weak var avatarImgVw: DesignableImageView!
    
    @IBOutlet weak var userNameLbl: UILabel!
    @IBOutlet weak var userDescLbl: UILabel!
    @IBOutlet weak var trackTableVw: UITableView!
    
    
    // GET THE USERS INFO
    
    override func viewDidAppear(animated: Bool) {
        
        realmMethods.getUserSoundCloudInfo(avatarImgVw, titleLbl: userNameLbl, descLbl: userDescLbl)
        spinnerMethods.ShowLoader()
        realmMethods.getUsersTracks() {
            (result: SwiftyJSON.JSON) in
              self.tracks = result
            //self.setTracks(result)
            
            self.trackTableVw.emptyDataSetSource = self
            self.trackTableVw.emptyDataSetDelegate = self
            self.trackTableVw.tableFooterView = UIView()
            
            self.trackTableVw.reloadData()
            
            
            self.trackTableVw.estimatedRowHeight = 120.0
            self.trackTableVw.rowHeight = UITableViewAutomaticDimension
            self.spinnerMethods.hideLoader()
            
        }
        
        
        
     
    }
    
    func setTracks(result: JSON){
        tracks = result
    }
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: TABLEWVIEW
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      print(tracks.count)
        return tracks.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCellWithIdentifier("trackCell") as! TrackTableViewCell
        
        // Get the track
       cell.configureTrack(tracks[indexPath.row], indexPath: indexPath)
        
       cell.trackPlayButton.addTarget(self, action: "playAction:", forControlEvents: .TouchUpInside)
        
        return cell
    }

    
    func playAction(sender: TrackTableViewCell){
        
    
        
        let songURl = tracks[sender.tag]["permalink_url"].string
        
        let svc = SFSafariViewController(URL: NSURL(string: songURl!)!, entersReaderIfAvailable: true)
        svc.delegate = self
        self.presentViewController(svc, animated: true, completion: nil)
        
    }
    
    func safariViewControllerDidFinish(controller: SFSafariViewController)
    {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    
    
    // MARK: DZNEmptyDataStates
    
    func imageForEmptyDataSet(scrollView: UIScrollView!) -> UIImage! {
        
        return UIImage(named: "Soundcloud-Launch")
        
    }
    
    func titleForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        
        let attribs = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(18),
            NSForegroundColorAttributeName: UIColor.darkGrayColor()
        ]

        return NSAttributedString(string: "No Tracks", attributes: attribs)
    }
    
    func descriptionForEmptyDataSet(scrollView: UIScrollView!) -> NSAttributedString! {
        
        // Hold the error messsage
        var text = ""
        
        let para = NSMutableParagraphStyle()
        para.lineBreakMode = NSLineBreakMode.ByWordWrapping
        para.alignment = NSTextAlignment.Center
        
        let attribs = [
            NSFontAttributeName: UIFont.systemFontOfSize(14),
            NSForegroundColorAttributeName: UIColor.lightGrayColor(),
            NSParagraphStyleAttributeName: para
        ]
        
        return NSAttributedString(string: "Go like some tracks on soundcloud", attributes: attribs)
    }
    
    func buttonTitleForEmptyDataSet(scrollView: UIScrollView!, forState state: UIControlState) -> NSAttributedString! {
        
        let attribs = [
            NSFontAttributeName: UIFont.boldSystemFontOfSize(16),
            NSForegroundColorAttributeName: view.tintColor
        ]
        
        return NSAttributedString(string: "Refresh", attributes: attribs)
    }
    
    func emptyDataSetDidTapButton(scrollView: UIScrollView!) {
        
        spinnerMethods.ShowLoader()
        realmMethods.getUsersTracks() {
            (result: SwiftyJSON.JSON) in
            self.tracks = result
            
            self.trackTableVw.reloadData()
            self.spinnerMethods.hideLoader()
            
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    

}

