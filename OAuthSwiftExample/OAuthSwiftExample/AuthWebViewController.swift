//
//  WebView.swift
//  OAuthSwift
//
//  Created by Dongri Jin on 2/11/15.
//  Copyright (c) 2015 Dongri Jin. All rights reserved.
//

import OAuthSwift
import UIKit
import SwiftLoader

typealias WebView = UIWebView


class AuthWebViewController: OAuthWebViewController{
    
    let spinner = Spinner()
    var targetURL : NSURL = NSURL()
    let webView : WebView = WebView()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Add top margin to the webview
          self.webView.scrollView.contentInset = UIEdgeInsets(top: 20, left: 0, bottom: 0, right: 0)
        
        // Attach new webview programatically
          self.webView.frame = UIScreen.mainScreen().bounds
          self.webView.scalesPageToFit = true
          self.webView.delegate = self
          self.view.addSubview(self.webView)
          loadAddressURL()
    }
    
    override func handle(url: NSURL) {
        targetURL = url
        super.handle(url)
        
        loadAddressURL()
    }
    
    func loadAddressURL() {
        let req = NSURLRequest(URL: targetURL)
        self.webView.loadRequest(req)
    }
}

// MARK: delegate
extension AuthWebViewController: UIWebViewDelegate {
    func webView(webView: UIWebView, shouldStartLoadWithRequest request: NSURLRequest, navigationType: UIWebViewNavigationType) -> Bool {
        if let url = request.URL where (url.scheme == "oauth-swift"){
            self.dismissWebViewController()
        }
        return true
    }
    
    func webViewDidFinishLoad(webView: UIWebView) {
        SwiftLoader.hide()
    }
}

