//
//  Realm.swift
//  OAuthSwiftExample
//
//  Created by Tunde Adegoroye on 07/07/2016.
//  Copyright © 2016 Tunde Adegoroye. All rights reserved.
//

import UIKit
import RealmSwift
import OAuthSwift
import SwiftyJSON
import Spring

class UserInfoDefault: Object {
    
    dynamic var id = 0
    dynamic var name = ""
    dynamic var avatar_url = ""
    dynamic var desc = ""
    dynamic var accessToken = ""
}

class RealmData: UIViewController {
    
    var usersPlaylists:JSON?
    func addDefaultUserInfo(id: Int, name: String, avatar_url: String, description: String ,accessToken: String) {
        
        // Create a User object
        let user = UserInfoDefault()
        user.id = id
        user.name = name
        user.avatar_url = avatar_url
        user.desc = description
        user.accessToken = accessToken
        
        do {
            
            let realm = try! Realm()
            
            try! realm.write {
                realm.add(user)
            }
            
        } catch {
            print(error)
        }
        
    }
    
    func getUserSoundCloudInfo(image: DesignableImageView, titleLbl: UILabel, descLbl: UILabel){
        
        
        
        do {
            
            let oauthswift = OAuth2Swift(
                consumerKey:    "20bc9e7db8e40065bc0331077326aa9e",
                consumerSecret: "b50d65498c712c0221a31706c0c210a1",
                authorizeUrl:   "https://soundcloud.com/connect",
                accessTokenUrl: "https://api.soundcloud.com/oauth2/token",
                responseType:   "code"
            )
            
            
            let realm = try! Realm()
            let userInfo = realm.objects(UserInfoDefault.self)
            
            // Just for testing purposes

            for user in userInfo {
                
                titleLbl.text = user.name
                descLbl.text = user.desc
                image.image = getImageFromURL(user.avatar_url)

            }
            
        } catch {
            print(error)
        }
        
    }
    
    
    func getUsersTracks(completion: (result: SwiftyJSON.JSON) -> Void) {
      
        
        
        do {
            
            let oauthswift = OAuth2Swift(
                consumerKey:    "20bc9e7db8e40065bc0331077326aa9e",
                consumerSecret: "b50d65498c712c0221a31706c0c210a1",
                authorizeUrl:   "https://soundcloud.com/connect",
                accessTokenUrl: "https://api.soundcloud.com/oauth2/token",
                responseType:   "code"
            )
            
            
            let realm = try! Realm()
            let userInfo = realm.objects(UserInfoDefault.self)
            
            // Just for testing purposes
           // var i = 0
            for user in userInfo {
                
              //  if i == 0 {
                    
                    oauthswift.client.get("https://api.soundcloud.com/users/\(user.id)/favorites/?client_id=20bc9e7db8e40065bc0331077326aa9e&limit=200",
                                          success: {
                                            data, response in
                                            self.usersPlaylists = SwiftyJSON.JSON(data: data)
                                            completion(result: self.usersPlaylists!)
                                            
                        }, failure: { error in
                            print(error)
                    })
                    
              //  }
               // i += 1
            }
            
        } catch {
            print(error)
        }
        
    }
    
    func isUserRegistered() -> Bool {
     
        var status = false
        
        do {
            
            let realm = try! Realm()
            let userInfo = realm.objects(UserInfoDefault.self)
            
            // Just for testing purposes
            for user in userInfo {
                
                if !user.accessToken.isEmpty {
                
                    status = true
                }
            }
            
            
            
            
        } catch {
            print(error)
        }

        return status
    }
    
    
    func getImageFromURL(imgURL: String) -> UIImage {
        if let url  = NSURL(string: imgURL),
            data = NSData(contentsOfURL: url)
        {
            return UIImage(data: data)!
        }
        
        return UIImage()
    }
}
